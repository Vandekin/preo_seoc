library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library work;

entity tb_mmips_simple  is
end tb_mmips_simple ;

architecture behavior of tb_mmips_simple is 

  component MMIPS_simple
    port (
      clk       : in  std_logic;
      reset     : in  std_logic;

      led       : out  std_logic_vector(7 downto 0);
      push      : in  std_logic_vector(1 downto 0)
      );
  end component;

  signal clk    :  std_logic:='0';
  signal reset  :  std_logic;
  signal led    :  std_logic_vector(7 downto 0);
  signal push   :  std_logic_vector(1 downto 0);
  
begin

  C_MMIPS_IO : MMIPS_simple
    port map(
      clk => clk ,
      reset => reset ,
      led       => led       ,
      push      => push   
      );
  
  process 
  begin
    clk<='1';
    wait for 10 ns;
    clk<='0';
    wait for 10 ns;
  end process;

  
  tb : process
  begin
    reset<='1';
    push<="00";
    for i in 1 to 6 loop    
      wait until rising_edge(clk);
    end loop;
    reset<='0';   
    

    wait; -- will wait forever
  end process;

end;
