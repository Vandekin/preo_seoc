library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library work;

entity tb_VGA_mire  is
end tb_VGA_mire ;

architecture behavior of tb_VGA_mire is 

  component VGA_Mire
    port (
      clk       : in  std_logic;
      reset     : in  std_logic;


      R         : out  STD_LOGIC;
      G         : out  STD_LOGIC;
      B         : out  STD_LOGIC;
      HS        : out  STD_LOGIC;
      VS        : out  STD_LOGIC
      );
  end component;

  signal clk    :  std_logic:='0';
  signal reset     :  STD_LOGIC;

  signal R      :  STD_LOGIC;
  signal G      :  STD_LOGIC;
  signal B      :  STD_LOGIC;
  signal HS     :  STD_LOGIC;
  signal VS  :  STD_LOGIC;
  
begin

  C_VGA_mire : VGA_Mire
    port map(
      clk => clk ,
      reset => reset ,
      
      R         => R         ,
      G         => G         ,
      B         => B         ,
      HS        => HS        ,
      VS        => VS        
      );
  
  process 
  begin
    clk<='1';
    wait for 10 ns;
    clk<='0';
    wait for 10 ns;
  end process;

  reset<= '1', '0' after 133 ns;
  
end;
