library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library work;

entity tb_mmips_IO  is
end tb_mmips_IO ;

architecture behavior of tb_mmips_IO is 

  component MMIPS_IO
    port (
      clk       : in  std_logic;
      reset     : in  std_logic;

      switch    : in  std_logic_vector(7 downto 0);
      push      : in  std_logic_vector(2 downto 0);
      led       : out  std_logic_vector(7 downto 0);
      seg       : out  std_logic_vector(7 downto 0);
      seg_an    : out  std_logic_vector(3 downto 0)
      );
  end component;

  signal clk    :  std_logic:='0';
  signal reset  :  std_logic;
  signal switch :  std_logic_vector(7 downto 0);
  signal push   :  std_logic_vector(2 downto 0);
  signal led    :  std_logic_vector(7 downto 0);
  signal seg    :  std_logic_vector(7 downto 0);
  signal seg_an :  std_logic_vector(3 downto 0);

begin

  C_MMIPS_IO : MMIPS_IO
    port map(
      clk => clk ,
      reset => reset ,
      switch    => switch    ,
      push      => push      ,
      led       => led       ,
      seg       => seg       ,
      seg_an    => seg_an   
      );
  
  process 
  begin
    clk<='1';
    wait for 10 ns;
    clk<='0';
    wait for 10 ns;
  end process;

  
  tb : process
  begin
    reset<='1';
    switch<=x"03";
    push<="000";
    for i in 1 to 6 loop    
      wait until rising_edge(clk);
    end loop;
    reset<='0';   
    switch<=x"06";
    push<="000";
    for i in 0 to 10 loop    
      wait until rising_edge(clk);
      switch <= switch + 1 ; 
    end loop;

    -- place stimulus here

    wait; -- will wait forever
  end process;

end;
