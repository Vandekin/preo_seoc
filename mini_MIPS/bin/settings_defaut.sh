#!/bin/bash

# Attention il faut mettre en place la config de Xilinx
# exemple a l'ENSIMAG : source /Xilinx/env-12.2.sh
export MMIPS=$(pwd)  # chemin complet du repertoire mini_MIPS
export MIPSC=mips-sde-elf-   # le prefix des binutils ou du crosscompilateur 
export PATH=$PATH:/opt/mips-toolchain-bin/bin 
# le PATH doit contenir le chemin de:
# - les binutils 
# - le cross-compilateur (inclue les binutils)

export PATH=$PATH:$MMIPS/bin/:$MMIPS/../binutils/

