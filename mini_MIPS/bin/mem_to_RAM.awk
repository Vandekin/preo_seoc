BEGIN {
  if (!w) 
    w=4; 
  addr=0;
  if (!offset) offset="0";
  o=strtonum("0x" offset);
}
/^@[[:xdigit:]]*/ {
  addr=strtonum("0x" substr($1,2));
  fl="";
}
!/^@/ { 
  if (fl != "") print fl ","; 
  fl=sprintf("%d => x\"%s\"", (addr-o)/w, $1);
  addr+=w;
}
END {print fl ","}

