library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity VGA_lecture_memoire is
  Port (
    clk : in  std_logic;
    rst : in  std_logic;
    addr : out std_logic_vector(11 downto 0);
    data : in std_logic_vector(31 downto 0);
    R : out  STD_LOGIC;
    G : out  STD_LOGIC;
    B : out  STD_LOGIC;
    HS : out  STD_LOGIC;
    VS : out  STD_LOGIC
    );
end VGA_lecture_memoire;

architecture RTL of VGA_lecture_memoire is

  signal xi: std_logic_vector(9 downto 0);
  signal yi: std_logic_vector(8 downto 0);
  signal pixel: std_logic;
  signal imgi: std_logic;
  signal  hs_d, vs_d : std_logic;

  component VGA_gene_sync 
    Port (
      clk : in std_logic;
      rst : in std_logic;
      hsync : out std_logic;
      vsync : out std_logic;
      img : out std_logic;
      x : out std_logic_vector(9 downto 0);
      y : out std_logic_vector(8 downto 0)
      );
  end component;

  
  
begin

  C_GeneSync : VGA_gene_sync 
    port map ( clk => clk, rst => rst, hsync=>hs_d, vsync=>vs_d, img=>imgi, x=>xi, y=>yi);
-- A COMPLETER et MODIFIER

R <= '0';     
G <= '0';   
B <= '0';   
HS <= '0'; 
VS <= '0';
  
end RTL;

