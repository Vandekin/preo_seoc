library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity VGA_image is
  Port ( CLK : in  STD_LOGIC;
	 RESET : in  STD_LOGIC;
	 R : out  STD_LOGIC;
	 G : out  STD_LOGIC;
	 B : out  STD_LOGIC;
	 HS : out  STD_LOGIC;
	 VS : out  STD_LOGIC
	 );
end VGA_image;

architecture RTL of VGA_image is 


  component VGA_lecture_memoire 
    Port (
      CLK : in  STD_LOGIC;
      RST : in  STD_LOGIC;
      ADDR : out std_logic_vector(11 downto 0);
      DATA : in std_logic_vector(31 downto 0);
      R : out  STD_LOGIC;
      G : out  STD_LOGIC;
      B : out  STD_LOGIC;
      HS : out  STD_LOGIC;
      VS : out  STD_LOGIC
      );
  end component;
  component RAM_Video is
    port ( 
      clka  : in    std_logic;   
      addra : in    std_logic_vector (11 downto 0); 
      doa   : out   std_logic_vector(31 downto 0);
      dia   : in   std_logic_vector(31 downto 0);
      wea   : in   std_logic;

      clkb  : in    std_logic;   
      addrb : in    std_logic_vector (11 downto 0); 
      dob   : out   std_logic_vector(31 downto 0);
      dib   : in   std_logic_vector(31 downto 0);
      web   : in   std_logic
      );
  end component RAM_Video;
  signal ADDR  : std_logic_vector(11 downto 0);
  signal DATA: std_logic_vector(31 downto 0);
 
  component  clock_gen
    port (
      clkin_in  : in    std_logic; 
      rst_in    : in    std_logic; 
      clk_cpu : out   std_logic; 
      clk_video : out   std_logic; 
      rst_out   : out   std_logic
      );
  end component clock_gen;
  signal clk25, rst :  std_logic;
begin
 
  C_DCM : clock_gen
    port map(
      clkin_in  => clk,
      rst_in    => reset, 
      clk_cpu   => clk25,

      rst_out   => rst   
      );

  
  VGA_lm: VGA_lecture_memoire
    Port map (
      CLK => CLK25,
      RST => RST,
      ADDR => ADDR,
      DATA => DATA,
      R => R,
      G => G,
      B => B,
      HS => HS,
      VS => VS 
      );
  
  C_RAM_Video : RAM_Video
    Port map  ( 
      clka  => clk25 ,
      addra => addr,
      doa   => data,
      dia   => x"00000000"  ,
      wea   => '0'  ,

      clkb  => clk25 ,
      addrb => addr,
      dob   => open ,
      dib   => x"00000000",
      web   => '0' 
      );
  
end RTL;
