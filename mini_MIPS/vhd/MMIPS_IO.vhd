----------------------------------------------------------------------------------
-- Company: Ensimag
-- Engineers: S. Mancini
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.MMIPS_pkg.all;

entity MMIPS_IO is
  port (
    clk    : in  std_logic;
    reset  : in  std_logic;

    switch : in  std_logic_vector(7 downto 0);
    push   : in  std_logic_vector(2 downto 0);
    led    : out  std_logic_vector(7 downto 0);
    seg    : out  std_logic_vector(7 downto 0);
    seg_an : out  std_logic_vector(3 downto 0)    
    );
end MMIPS_IO;

architecture structural of MMIPS_IO is

  

  signal mem_addr    :  waddr;
  signal mem_datain  :  w32;
  signal mem_dataout :  w32;
  
  signal mem_we      :  std_logic;
  signal mem_ce      :  std_logic;
-- A MODIFIER

  constant nCE       : integer := 2;
  signal bus_ce, bus_we       : std_logic_vector(0 to nce-1);
  signal bus_datai       :  w32_vec(0 to nce-1);
  signal bus_addr        :  waddr;


  signal push_tmp    :  w32;

  signal clk25, dcm_lock, rst :  std_logic;
  
begin
  
  C_DCM: clock_gen
    port map(
      clkin_in => clk,
      rst_in   => reset, 
      clk_cpu  => clk25,
      rst_out  => rst   
      );
  
  C_PROC: MMIPS_CPU
    port map(
      clk         => clk25,
      rst         => rst,
      
      irq         => '0',
      
      mem_addr    => mem_addr,
      mem_datain  => mem_datain,
      mem_dataout => mem_dataout,

      mem_we      => mem_we,
      mem_ce      => mem_ce,

      pout        => open,    
      pout_valid  => open    
      );
  
  -- MEM_PROG led sw&push seg  timer
  C_bus : MMIPS_bus
    generic map(
      nCE => nCE,
-- A MODIFIER
      base => (x"000000", x"004000"
               ), 
      high => (x"001FFF", x"004007"
               )
      )
    port map(
      clk       => clk25,
      rst       => rst,

      cpu_ce    => mem_ce,
      cpu_we    => mem_we,
      cpu_addr  => mem_addr,
      cpu_datao => mem_dataout,
      cpu_datai => mem_datain,

      ce        => bus_ce,
      we        => bus_we,
      addr      => bus_addr,
      datai     => bus_datai 
      );

  C_RAM_PROG: RAM_PROG
    port map(
      clk  => clk25,

      addr => mem_addr(12 downto 2),
      do   => bus_datai(0),
      di   => mem_dataout,
      we   => bus_we(0)
      );
  
  
  C_LED     : IP_LED
    port map(
      clk   => clk25,
      rst   => rst,
      -- bus
      we    => bus_we(1),
      ce    => bus_ce(1),
      ad    => bus_addr,
      datai => mem_dataout,
      datao => bus_datai(1),
      -- io
      led   => led 
      );
-- A COMPLETER ET MODIFIER

seg <= (others=>'0');
seg_an <= (others=>'0');
end structural;

