----------------------------------------------------------------------------------
-- Company: Ensimag
-- Engineers: S. Mancini
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.MMIPS_pkg.all;

entity IP_VGA is
  port (
    clk : in std_logic;
    rst: in std_logic; 
    -- bus
    we : in std_logic;
    ce : in std_logic;
    ad : in waddr;
    datai : in w32;
    datao : out w32;
    -- io
    r,g,b, hs, vs  : out std_logic
    );
end IP_VGA  ;

architecture RTL of IP_VGA  is

  signal video_addr : std_logic_vector(11 downto 0);
  signal video_data : w32;


begin
  C_RAM_VIDEO : RAM_Video 
    port map( 

      clka  => clk ,
      addra => ad(13 downto 2),
      doa   => datao  ,
      dia   => datai ,
      wea   => we  ,

      clkb  => clk,
      addrb => video_addr,
      dob   => video_data ,
      dib   => x"00000000"  ,
      web   => '0'   

      );

  C_VGA_lecture_memoire  : VGA_lecture_memoire 
    port map(
      clk => clk,
      rst => rst,
      addr => video_addr,
      data => video_data,
      r => r,
      g => g,
      b => b,
      hs => hs,
      vs => vs 
      );

end RTL;
