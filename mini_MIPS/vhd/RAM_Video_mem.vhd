library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
--use UNISIM.Vcomponents.ALL;
use work.MMIPS_pkg.all;
use work.RAM_Video_data_pkg.all;

-- pour une image 512*256 -> 4096 mots de 32 bits

entity RAM_Video is
  port ( 
    clka  : in    std_logic;   
    addra : in    std_logic_vector (11 downto 0); 
    doa   : out   std_logic_vector(31 downto 0);
    dia   : in   std_logic_vector(31 downto 0);
    wea   : in   std_logic;

    clkb  : in    std_logic;   
    addrb : in    std_logic_vector (11 downto 0); 
    dob   : out   std_logic_vector(31 downto 0);
    dib   : in   std_logic_vector(31 downto 0);
    web   : in   std_logic
    );
end RAM_Video;

architecture BEHAVIORAL of RAM_Video is
  attribute INIT_A       : string ;
  attribute INIT_B       : string ;
  attribute SRVAL_A      : string ;
  attribute SRVAL_B      : string ;
  attribute INITP_00   : string ;
  attribute INITP_01   : string ;
  attribute INITP_02   : string ;
  attribute INITP_03   : string ;
  attribute INITP_04   : string ;
  attribute INITP_05   : string ;
  attribute INITP_06   : string ;
  attribute INITP_07   : string ;
  attribute WRITE_MODE_A : string ;
  attribute WRITE_MODE_B : string ;
  attribute BOX_TYPE   : string ;
  component RAMB16_S9_S9
    generic( INIT_A : bit_vector :=  x"000";
	     INIT_B : bit_vector :=  x"000";
	     SRVAL_A : bit_vector :=  x"000";
	     SRVAL_B : bit_vector :=  x"000";
	     WRITE_MODE_A : string :=  "WRITE_FIRST";
	     WRITE_MODE_B : string :=  "WRITE_FIRST";
	     INIT_00 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_00;
	     INIT_01 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_01;
	     INIT_02 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_02;
	     INIT_03 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_03;
	     INIT_04 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_04;
	     INIT_05 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_05;
	     INIT_06 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_06;
	     INIT_07 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_07;
	     INIT_08 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_08;
	     INIT_09 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_09;
	     INIT_0A : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_0a;
	     INIT_0B : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_0b;
	     INIT_0C : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_0c;
	     INIT_0D : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_0d;
	     INIT_0E : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_0e;
	     INIT_0F : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_0f;
	     INIT_10 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_10;
	     INIT_11 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_11;
	     INIT_12 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_12;
	     INIT_13 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_13;
	     INIT_14 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_14;
	     INIT_15 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_15;
	     INIT_16 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_16;
	     INIT_17 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_17;
	     INIT_18 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_18;
	     INIT_19 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_19;
	     INIT_1A : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_1a;
	     INIT_1B : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_1b;
	     INIT_1C : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_1c;
	     INIT_1D : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_1d;
	     INIT_1E : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_1e;
	     INIT_1F : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_1f;
	     INIT_20 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_20;
	     INIT_21 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_21;
	     INIT_22 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_22;
	     INIT_23 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_23;
	     INIT_24 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_24;
	     INIT_25 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_25;
	     INIT_26 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_26;
	     INIT_27 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_27;
	     INIT_28 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_28;
	     INIT_29 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_29;
	     INIT_2A : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_2a;
	     INIT_2B : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_2b;
	     INIT_2C : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_2c;
	     INIT_2D : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_2d;
	     INIT_2E : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_2e;
	     INIT_2F : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_2f;
	     INIT_30 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_30;
	     INIT_31 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_31;
	     INIT_32 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_32;
	     INIT_33 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_33;
	     INIT_34 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_34;
	     INIT_35 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_35;
	     INIT_36 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_36;
	     INIT_37 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_37;
	     INIT_38 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_38;
	     INIT_39 : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_39;
	     INIT_3A : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_3a;
	     INIT_3B : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_3b;
	     INIT_3C : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_3c;
	     INIT_3D : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_3d;
	     INIT_3E : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_3e;
	     INIT_3F : bit_vector := C_VGA_C_RAM_Video_XLXI_0_INIT_3f;
	     INITP_00 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000";
	     INITP_01 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000";
	     INITP_02 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000";
	     INITP_03 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000";
	     INITP_04 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000";
	     INITP_05 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000";
	     INITP_06 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000";
	     INITP_07 : bit_vector :=  
	     x"0000000000000000000000000000000000000000000000000000000000000000");

    port (
      CLKA  : in    std_logic;
      ADDRA : in    std_logic_vector (10 downto 0);       
      DIA  : in    std_logic_vector (7 downto 0); 
      DIPA  : in    std_logic_vector (0 downto 0); 
      ENA   : in    std_logic; 
      SSRA  : in    std_logic; 
      WEA   : in    std_logic; 
      DOA   : out   std_logic_vector (7 downto 0); 
      DOPA  : out   std_logic_vector (0 downto 0);
      
      CLKB  : in    std_logic; 
      ADDRB : in    std_logic_vector (10 downto 0); 
      DIB   : in    std_logic_vector (7 downto 0); 
      DIPB  : in    std_logic_vector (0 downto 0); 
      ENB   : in    std_logic; 
      SSRB  : in    std_logic; 
      WEB   : in    std_logic; 
      DOB   : out   std_logic_vector (7 downto 0); 
      DOPB  : out   std_logic_vector (0 downto 0)

      );
  end component;
  attribute INIT_A of RAMB16_S9_S9 : component is "000";
  attribute INIT_B of RAMB16_S9_S9 : component is "000";
  attribute SRVAL_A of RAMB16_S9_S9 : component is "000";
  attribute SRVAL_B of RAMB16_S9_S9 : component is "000";
  attribute WRITE_MODE_A of RAMB16_S9_S9 : component is "WRITE_FIRST";
  attribute WRITE_MODE_B of RAMB16_S9_S9 : component is "WRITE_FIRST";

  attribute INITP_00 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  attribute INITP_01 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  attribute INITP_02 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  attribute INITP_03 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  attribute INITP_04 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  attribute INITP_05 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  attribute INITP_06 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  attribute INITP_07 of RAMB16_S9_S9 : component is 
    "0000000000000000000000000000000000000000000000000000000000000000";
  
  attribute BOX_TYPE of RAMB16_S9_S9 : component is "BLACK_BOX";

  signal ena_up, enb_up : std_logic;
  signal ena_down, enb_down : std_logic;
--  signal ena_up_q, enb_up_q : std_logic;
  signal ena_down_q, enb_down_q : std_logic;
  signal doa_down, doa_up, dob_down, dob_up : std_logic_vector(31 downto 0);

begin
  ena_up <= addra(11);
  ena_down <= not addra(11);
  
  process (clka)
  begin 
    if rising_edge(clka) then
--      ena_up_q <= ena_up;
      ena_down_q <= ena_down;
    end if;
  end process;
  DOA <= doa_down
	 when ena_down_q='1' else
	 doa_up;
  
  enb_up <= addrb(11);
  enb_down <= not addrb(11);

  process (clkb)
  begin
    if rising_edge(clkb) then
--      enb_up_q <= enb_up;
      enb_down_q <= enb_down;
    end if;
  end process;
  
  DOB <= dob_down
	 when enb_down_q='1' else
	 dob_up;
  
  XLXI_0 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_0_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_0_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_0_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_0_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_0_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_0_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_0_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_0_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_0_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_0_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_0_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_0_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_0_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_0_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_0_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_0_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_0_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_0_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_0_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_0_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_0_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_0_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_0_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_0_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_0_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_0_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_0_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_0_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_0_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_0_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_0_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_0_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_0_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_0_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_0_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_0_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_0_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_0_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_0_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_0_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_0_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_0_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_0_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_0_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_0_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_0_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_0_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_0_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_0_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_0_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_0_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_0_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_0_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_0_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_0_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_0_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_0_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_0_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_0_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_0_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_0_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_0_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_0_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_0_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(7 downto 0),
      DIPA(0) =>'0',
      ENA     => ena_down,
      SSRA    =>'0',
      WEA     => weA,
      DOA     =>doa_down(7 downto 0),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(7 downto 0),
      DIPB(0) =>'0',
      ENB     => enb_down,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_down(7 downto 0),
      DOPB=>open

      );
  
  XLXI_1 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_1_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_1_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_1_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_1_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_1_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_1_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_1_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_1_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_1_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_1_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_1_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_1_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_1_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_1_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_1_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_1_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_1_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_1_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_1_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_1_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_1_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_1_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_1_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_1_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_1_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_1_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_1_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_1_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_1_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_1_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_1_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_1_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_1_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_1_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_1_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_1_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_1_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_1_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_1_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_1_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_1_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_1_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_1_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_1_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_1_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_1_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_1_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_1_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_1_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_1_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_1_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_1_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_1_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_1_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_1_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_1_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_1_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_1_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_1_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_1_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_1_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_1_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_1_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_1_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(15 downto 8),
      DIPA(0) =>'0',
      ENA     => ena_down,
      SSRA    =>'0',
      WEA     => weA,
      DOA     =>doa_down(15 downto 8),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(15 downto 8),
      DIPB(0) =>'0',
      ENB     => enb_down,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_down(15 downto 8),
      DOPB=>open
      );

  XLXI_2 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_2_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_2_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_2_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_2_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_2_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_2_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_2_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_2_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_2_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_2_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_2_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_2_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_2_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_2_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_2_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_2_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_2_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_2_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_2_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_2_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_2_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_2_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_2_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_2_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_2_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_2_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_2_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_2_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_2_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_2_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_2_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_2_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_2_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_2_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_2_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_2_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_2_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_2_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_2_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_2_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_2_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_2_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_2_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_2_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_2_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_2_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_2_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_2_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_2_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_2_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_2_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_2_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_2_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_2_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_2_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_2_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_2_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_2_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_2_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_2_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_2_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_2_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_2_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_2_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(23 downto 16),
      DIPA(0) =>'0',
      ENA     => ena_down,
      SSRA    =>'0',
      WEA     => weA,
      DOA     =>doa_down(23 downto 16),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(23 downto 16),
      DIPB(0) =>'0',
      ENB     => enb_down,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_down(23 downto 16),
      DOPB=>open
      );

  
  XLXI_3 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_3_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_3_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_3_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_3_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_3_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_3_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_3_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_3_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_3_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_3_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_3_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_3_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_3_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_3_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_3_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_3_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_3_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_3_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_3_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_3_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_3_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_3_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_3_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_3_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_3_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_3_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_3_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_3_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_3_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_3_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_3_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_3_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_3_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_3_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_3_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_3_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_3_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_3_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_3_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_3_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_3_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_3_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_3_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_3_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_3_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_3_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_3_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_3_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_3_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_3_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_3_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_3_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_3_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_3_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_3_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_3_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_3_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_3_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_3_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_3_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_3_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_3_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_3_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_3_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(31 downto 24),
      DIPA(0) =>'0',
      ENA     => ena_down,
      SSRA    =>'0',
      WEA     => weA,
      DOA     => doa_down(31 downto 24),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(31 downto 24),
      DIPB(0) =>'0',
      ENB     => enb_down,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_down(31 downto 24),
      DOPB=>open
      );
  XLXI_4 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_4_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_4_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_4_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_4_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_4_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_4_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_4_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_4_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_4_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_4_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_4_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_4_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_4_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_4_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_4_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_4_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_4_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_4_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_4_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_4_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_4_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_4_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_4_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_4_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_4_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_4_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_4_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_4_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_4_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_4_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_4_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_4_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_4_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_4_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_4_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_4_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_4_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_4_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_4_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_4_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_4_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_4_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_4_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_4_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_4_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_4_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_4_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_4_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_4_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_4_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_4_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_4_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_4_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_4_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_4_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_4_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_4_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_4_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_4_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_4_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_4_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_4_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_4_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_4_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(7 downto 0),
      DIPA(0) =>'0',
      ENA     => ena_up,
      SSRA    =>'0',
      WEA     => weA,
      DOA     =>doa_up(7 downto 0),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(7 downto 0),
      DIPB(0) =>'0',
      ENB     => enb_up,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_up(7 downto 0),
      DOPB=>open

      );
  XLXI_5 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_5_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_5_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_5_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_5_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_5_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_5_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_5_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_5_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_5_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_5_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_5_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_5_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_5_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_5_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_5_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_5_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_5_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_5_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_5_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_5_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_5_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_5_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_5_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_5_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_5_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_5_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_5_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_5_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_5_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_5_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_5_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_5_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_5_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_5_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_5_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_5_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_5_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_5_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_5_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_5_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_5_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_5_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_5_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_5_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_5_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_5_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_5_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_5_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_5_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_5_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_5_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_5_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_5_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_5_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_5_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_5_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_5_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_5_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_5_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_5_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_5_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_5_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_5_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_5_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(15 downto 8),
      DIPA(0) =>'0',
      ENA     =>ena_up,
      SSRA    =>'0',
      WEA     => weA,
      DOA     =>doa_up(15 downto 8),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(15 downto 8),
      DIPB(0) =>'0',
      ENB     =>enb_up,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_up(15 downto 8),
      DOPB=>open

      );
  XLXI_6 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_6_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_6_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_6_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_6_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_6_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_6_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_6_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_6_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_6_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_6_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_6_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_6_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_6_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_6_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_6_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_6_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_6_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_6_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_6_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_6_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_6_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_6_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_6_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_6_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_6_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_6_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_6_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_6_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_6_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_6_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_6_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_6_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_6_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_6_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_6_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_6_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_6_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_6_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_6_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_6_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_6_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_6_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_6_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_6_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_6_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_6_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_6_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_6_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_6_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_6_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_6_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_6_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_6_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_6_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_6_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_6_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_6_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_6_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_6_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_6_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_6_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_6_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_6_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_6_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(23 downto 16),
      DIPA(0) =>'0',
      ENA     =>ena_up,
      SSRA    =>'0',
      WEA     => weA,
      DOA     =>doa_up(23 downto 16),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(23 downto 16),
      DIPB(0) =>'0',
      ENB     =>enb_up,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_up(23 downto 16),
      DOPB=>open

      );
  XLXI_7 : RAMB16_S9_S9
    generic map (
      INIT_00=>C_VGA_C_RAM_Video_XLXI_7_INIT_00,
      INIT_01=>C_VGA_C_RAM_Video_XLXI_7_INIT_01,
      INIT_02=>C_VGA_C_RAM_Video_XLXI_7_INIT_02,
      INIT_03=>C_VGA_C_RAM_Video_XLXI_7_INIT_03,
      INIT_04=>C_VGA_C_RAM_Video_XLXI_7_INIT_04,
      INIT_05=>C_VGA_C_RAM_Video_XLXI_7_INIT_05,
      INIT_06=>C_VGA_C_RAM_Video_XLXI_7_INIT_06,
      INIT_07=>C_VGA_C_RAM_Video_XLXI_7_INIT_07,
      INIT_08=>C_VGA_C_RAM_Video_XLXI_7_INIT_08,
      INIT_09=>C_VGA_C_RAM_Video_XLXI_7_INIT_09,
      INIT_0a=>C_VGA_C_RAM_Video_XLXI_7_INIT_0A,
      INIT_0b=>C_VGA_C_RAM_Video_XLXI_7_INIT_0B,
      INIT_0c=>C_VGA_C_RAM_Video_XLXI_7_INIT_0C,
      INIT_0d=>C_VGA_C_RAM_Video_XLXI_7_INIT_0D,
      INIT_0e=>C_VGA_C_RAM_Video_XLXI_7_INIT_0E,
      INIT_0f=>C_VGA_C_RAM_Video_XLXI_7_INIT_0F,
      INIT_10=>C_VGA_C_RAM_Video_XLXI_7_INIT_10,
      INIT_11=>C_VGA_C_RAM_Video_XLXI_7_INIT_11,
      INIT_12=>C_VGA_C_RAM_Video_XLXI_7_INIT_12,
      INIT_13=>C_VGA_C_RAM_Video_XLXI_7_INIT_13,
      INIT_14=>C_VGA_C_RAM_Video_XLXI_7_INIT_14,
      INIT_15=>C_VGA_C_RAM_Video_XLXI_7_INIT_15,
      INIT_16=>C_VGA_C_RAM_Video_XLXI_7_INIT_16,
      INIT_17=>C_VGA_C_RAM_Video_XLXI_7_INIT_17,
      INIT_18=>C_VGA_C_RAM_Video_XLXI_7_INIT_18,
      INIT_19=>C_VGA_C_RAM_Video_XLXI_7_INIT_19,
      INIT_1a=>C_VGA_C_RAM_Video_XLXI_7_INIT_1A,
      INIT_1b=>C_VGA_C_RAM_Video_XLXI_7_INIT_1B,
      INIT_1c=>C_VGA_C_RAM_Video_XLXI_7_INIT_1C,
      INIT_1d=>C_VGA_C_RAM_Video_XLXI_7_INIT_1D,
      INIT_1e=>C_VGA_C_RAM_Video_XLXI_7_INIT_1E,
      INIT_1f=>C_VGA_C_RAM_Video_XLXI_7_INIT_1F,
      INIT_20=>C_VGA_C_RAM_Video_XLXI_7_INIT_20,
      INIT_21=>C_VGA_C_RAM_Video_XLXI_7_INIT_21,
      INIT_22=>C_VGA_C_RAM_Video_XLXI_7_INIT_22,
      INIT_23=>C_VGA_C_RAM_Video_XLXI_7_INIT_23,
      INIT_24=>C_VGA_C_RAM_Video_XLXI_7_INIT_24,
      INIT_25=>C_VGA_C_RAM_Video_XLXI_7_INIT_25,
      INIT_26=>C_VGA_C_RAM_Video_XLXI_7_INIT_26,
      INIT_27=>C_VGA_C_RAM_Video_XLXI_7_INIT_27,
      INIT_28=>C_VGA_C_RAM_Video_XLXI_7_INIT_28,
      INIT_29=>C_VGA_C_RAM_Video_XLXI_7_INIT_29,
      INIT_2a=>C_VGA_C_RAM_Video_XLXI_7_INIT_2A,
      INIT_2b=>C_VGA_C_RAM_Video_XLXI_7_INIT_2B,
      INIT_2c=>C_VGA_C_RAM_Video_XLXI_7_INIT_2C,
      INIT_2d=>C_VGA_C_RAM_Video_XLXI_7_INIT_2D,
      INIT_2e=>C_VGA_C_RAM_Video_XLXI_7_INIT_2E,
      INIT_2f=>C_VGA_C_RAM_Video_XLXI_7_INIT_2F,
      INIT_30=>C_VGA_C_RAM_Video_XLXI_7_INIT_30,
      INIT_31=>C_VGA_C_RAM_Video_XLXI_7_INIT_31,
      INIT_32=>C_VGA_C_RAM_Video_XLXI_7_INIT_32,
      INIT_33=>C_VGA_C_RAM_Video_XLXI_7_INIT_33,
      INIT_34=>C_VGA_C_RAM_Video_XLXI_7_INIT_34,
      INIT_35=>C_VGA_C_RAM_Video_XLXI_7_INIT_35,
      INIT_36=>C_VGA_C_RAM_Video_XLXI_7_INIT_36,
      INIT_37=>C_VGA_C_RAM_Video_XLXI_7_INIT_37,
      INIT_38=>C_VGA_C_RAM_Video_XLXI_7_INIT_38,
      INIT_39=>C_VGA_C_RAM_Video_XLXI_7_INIT_39,
      INIT_3a=>C_VGA_C_RAM_Video_XLXI_7_INIT_3A,
      INIT_3b=>C_VGA_C_RAM_Video_XLXI_7_INIT_3B,
      INIT_3c=>C_VGA_C_RAM_Video_XLXI_7_INIT_3C,
      INIT_3d=>C_VGA_C_RAM_Video_XLXI_7_INIT_3D,
      INIT_3e=>C_VGA_C_RAM_Video_XLXI_7_INIT_3E,
      INIT_3f=>C_VGA_C_RAM_Video_XLXI_7_INIT_3F
      )
    port map (
      CLKA    =>CLKA,
      ADDRA   =>ADDRA(10 downto 0),
      DIA     => diA(31 downto 24),
      DIPA(0) =>'0',
      ENA     =>ena_up,
      SSRA    =>'0',
      WEA     => weA,
      DOA     =>doa_up(31 downto 24),
      DOPA=>open,

      CLKB    =>CLKB,
      ADDRB   =>ADDRB(10 downto 0),
      DIB     => diB(31 downto 24),
      DIPB(0) =>'0',
      ENB     =>enb_up,
      SSRB    =>'0',
      WEB     => weB,
      DOB     =>dob_up(31 downto 24),
      DOPB=>open

      );
  
end BEHAVIORAL;


