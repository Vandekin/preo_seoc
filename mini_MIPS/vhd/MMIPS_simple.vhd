----------------------------------------------------------------------------------
-- Company: Ensimag
-- Engineers: S. Mancini
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.MMIPS_pkg.all;
entity MMIPS_simple is
  port (
    clk : in  std_logic;
    reset : in  std_logic;

    push : in  std_logic_vector(1 downto 0);
    led : out  std_logic_vector(7 downto 0)
    );
end MMIPS_simple;

architecture structural of MMIPS_simple is

  signal mem_addr          :  waddr;
  signal mem_datain      :  w32;
  signal mem_dataout     :  w32;
  
  signal mem_we        :  std_logic;
  signal mem_ce        :  std_logic;

  signal clk25, dcm_lock, rst :  std_logic;
  
  signal pout32     :  w32;

  
begin

  C_DCM: clock_gen
    port map(
      CLKIN_IN  => clk,
      RST_IN    => reset, 
      CLK_CPU => clk25,
      RST_OUT   => rst   
      );

  
  C_PROC: MMIPS_CPU
    port map(
      clk         => clk25,
      rst         => rst,
      
      irq         =>  '0',

      
      mem_addr    => mem_addr,
      mem_datain  => mem_datain,
      mem_dataout => mem_dataout,

      mem_we      => mem_we,
      mem_ce      => mem_ce,

      pout        => pout32    
      );

  sel_led : process(pout32, push)
  begin
    led <= pout32((conv_integer(push)+1)*8-1 downto conv_integer(push)*8);
  end process sel_led;
  
  C_RAM_PROG : RAM_PROG
    port map(
      clk  => clk25,

      addr => mem_addr(12 downto 2),
      do   => mem_datain,
      di   => mem_dataout,
      we   => mem_we   
      );

end structural;

