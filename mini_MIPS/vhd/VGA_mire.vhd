library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity VGA_mire is
  Port (
    clk : in  std_logic;
    reset : in  std_logic;
    R : out  STD_LOGIC;
    G : out  STD_LOGIC;
    B : out  STD_LOGIC;
    HS : out  STD_LOGIC;
    VS : out  STD_LOGIC
    );
end VGA_mire;

architecture RTL of VGA_mire is 



  signal xi: std_logic_vector(9 downto 0);
  signal yi: std_logic_vector(8 downto 0);
  signal pixel: std_logic;
  signal imgi: std_logic;
  component vga_gene_sync 
    port (
      clk : in std_logic;
      rst : in std_logic;
      hsync : out std_logic;
      vsync : out std_logic;
      img : out std_logic;
      x : out std_logic_vector(9 downto 0);
      y : out std_logic_vector(8 downto 0));
  end component;
   
  component  clock_gen
    port (
      clkin_in  : in    std_logic; 
      rst_in    : in    std_logic; 
      clk_cpu : out   std_logic; 
      clk_video : out   std_logic; 
      rst_out   : out   std_logic
      );
  end component clock_gen;
  signal clk25, rst :  std_logic;
  
begin
 C_DCM : clock_gen
    port map(
      clkin_in  => clk,
      rst_in    => reset, 
      clk_cpu   => clk25,

      rst_out   => rst   
      );
 
  C_GeneSync : VGA_gene_sync 
    port map (
      clk   => clk25,
      rst   => rst,
      hsync => hs,
      vsync => vs,
      img   => imgi,
      x     => xi,
      y     => yi
      );
-- A COMPLETER ET MODIFIER

R <= '0';     
G <= '0';   
B <= '0';   
  

end RTL;
