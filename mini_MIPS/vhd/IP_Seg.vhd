----------------------------------------------------------------------------------
-- Company: Ensimag
-- Engineers: S. Mancini
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.MMIPS_pkg.all;

entity IP_Seg is
  port (
    clk : in std_logic;
    rst: in std_logic; 
    -- bus
    we : in std_logic;
    ce : in std_logic;
    ad : in waddr;
    datai : in w32;
    datao : out w32;
    -- io
    seg : out std_logic_vector(7 downto 0);
    an : out std_logic_vector(3 downto 0)
    );
end IP_Seg  ;

architecture RTL of IP_Seg  is
-- A COMPLETER

begin
  -- A COMPLETER
  datao <= (others=>'0');

end RTL;
