library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.MMIPS_pkg.all;

entity IP_LED is
  port (
    clk : in std_logic;
    rst: in std_logic; 
-- bus
    we : in std_logic;
    ce : in std_logic;
    ad : in waddr;
    datai : in w32;
    datao : out w32;
    -- io
    led : out std_logic_vector(7 downto 0)
    );
end IP_LED ;

architecture RTL of IP_LED  is

  

begin

  synchrone: process (clk)
    variable digit : integer range 0 to 3;  -- selection de l'octet affiche
    variable data : w32;  		-- sauvegarde du mot a afficher
  begin
    if clk'event and clk='1' then
      if (rst='1') then 
	led <= (others => '0');
	data := (others => '0');
	digit := 0;
      else
	if  ce='1' and we='1' then
	  if ad(2)='0' then
	    data := datai;
	  end if;
	  if ad(2)='1' then
	    digit := conv_integer(datai(1 downto 0)); 
	  end if;
	end if;
	led <= data(8*(digit+1)-1 downto 8*digit); 
      end if;
    end if;
  end process synchrone;

  datao <= (others=>'0');
  

end RTL;
