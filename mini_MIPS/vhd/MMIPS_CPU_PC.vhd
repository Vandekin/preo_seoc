---------------------------------------------------------
-- Company: Ensimag
-- Engineers: Mancini
---------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.MMIPS_pkg.all;
entity MMIPS_CPU_PC is
  Port (
    clk    : in  STD_LOGIC;
    rst    : in  STD_LOGIC;
    cmd    : out MMIPS_PO_cmd;
    status : in MMIPS_PO_status
    );
end MMIPS_CPU_PC;
architecture RTL of MMIPS_CPU_PC is
  type State_type is (S_Error,
                      S_Init,
                      S_Fetch_wait,
                      S_Fetch,
                      S_Decode,
                      S_LUI,
					  S_ORI,
					  S_BEQ1,
					  S_BEQ2,
					  S_BNE1,
					  S_BNE2,
					  S_ADDI,
					  S_ADD,
			S_AND,
			S_ANDI,
			S_XOR,
			S_OR,
			S_J,
			S_SLL
                      );

  signal state_d, state_q : State_type;
begin
  FSM_synchrone : process(clk)
  begin
    if clk'event and clk='1' then
      if rst='1' then
        state_q <= S_Init;
      else
        state_q <= state_d;
      end if;
    end if;
  end process FSM_synchrone;

  FSM_comb : process (state_q, status)
  begin
    state_d <= state_q;
    cmd <= MMIPS_PO_cmd_zero;
    case state_q is
      when S_Error =>
        state_d <= S_Error;
        
      when S_Init =>
        cmd.ALU_X_sel <= UXS_cst_x00;
        cmd.ALU_Y_sel <= UYS_cst_x00;
        cmd.ALU_OP <= AO_plus;
        cmd.PC_we <= true;
        state_d <= S_Fetch_wait;

      when S_Fetch_wait =>
        cmd.mem_ce <= true;
        state_d <= S_Fetch;

      when S_Fetch =>
        cmd.IR_we <= true;
        state_d <= S_Decode;
      when S_Decode =>	
        cmd.ALU_X_sel <= UXS_PC;
        cmd.ALU_Y_sel <= UYS_cst_x04;
        cmd.ALU_OP <= AO_plus;
        cmd.PC_we <= true;
        state_d <= S_Init;

        case status.IR(31 downto 29) is
		when "000" =>
			case status.IR(28 downto 26) is
			when "000" =>
				case status.IR(5 downto 3) is
					when "000" => 
						case status.IR(2 downto 0) is
							when "000" => state_d <= S_SLL;
							when others => null;
						end case;
					when "100" => 
						case status.IR(2 downto 0) is
							when "000" => state_d <= S_ADD;
							when "100" => state_d <= S_AND;
							when "110" => state_d <= S_XOR;
							when "101" => state_d <= S_OR;
							when others => null;
						end case;
					when others => null;
				end case;

			when "010" => state_d <= S_J;
			when "100" => state_d <= S_BEQ1;
			when "101" => state_d <= S_BNE1;
              	when others => null;
			end case;
          when "001" => 
            case status.IR(28 downto 26) is
              when "111" => state_d <= S_LUI;
	      when "101" => state_d <= S_ORI;
	      when "000" => state_d <= S_ADDI;
	      when "100" => state_d <= S_ANDI;
              when others => null;
            end case;
          when others => null;
        end case;
	

      when S_LUI =>
        	cmd.ALU_X_sel <= UXS_cst_x10;
        	cmd.ALU_Y_sel <= UYS_IR_imm16;
        	cmd.ALU_OP <= AO_SLL;
        	cmd.RF_Sel <= RFS_RT;
        	cmd.RF_we <= true;
        	state_d <= S_Fetch;
        	cmd.mem_ce <= true;
      when S_ORI =>
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_IR_imm16;
		cmd.ALU_OP <= AO_or;
		cmd.RF_Sel <= RFS_RT ;
		cmd.RF_we <= true ;
		state_d <= S_Fetch ;
		cmd.mem_ce <= true ;
      when S_BEQ1 =>
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_RF_RT;
		cmd.ALU_OP <= AO_moins;
		cmd.mem_ce <= true ;
		if status.z = false  then
			state_d <= S_Fetch ;
		else
			state_d <= S_BEQ2 ;
		end if;
      when S_BEQ2 =>
		cmd.ALU_X_sel <= UXS_PC;
		cmd.ALU_Y_sel <= UYS_IR_imm16_ext_up;
		cmd.ALU_OP <= AO_plus;
		cmd.PC_we <= true;
		state_d <= S_Fetch_wait;
		cmd.mem_ce <= true;
     when S_BNE1 => 
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_RF_RT;
		cmd.ALU_OP <= AO_moins;
		cmd.mem_ce <= true ;
		if status.z = true  then
			state_d <= S_Fetch ;
		else
			state_d <= S_BNE2 ;
		end if;
     when S_BNE2 =>
		cmd.ALU_X_sel <= UXS_PC;
		cmd.ALU_Y_sel <= UYS_IR_imm16_ext_up;
		cmd.ALU_OP <= AO_plus;
		cmd.PC_we <= true;
		state_d <= S_Fetch_wait;
		cmd.mem_ce <= true;
     when S_ADDI =>
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_IR_imm16_ext;
		cmd.ALU_OP <= AO_plus;
		cmd.RF_Sel <= RFS_RT;
		cmd.RF_we <= true;
		state_d <= S_Fetch;
		cmd.mem_ce <= true;
     when S_ADD =>
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_RF_RT;
		cmd.ALU_OP <= AO_plus;
		cmd.RF_sel <= RFS_RD;
		cmd.RF_we <= true;
		state_d <= S_Fetch;
		cmd.mem_ce <= true;
     when S_AND =>
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_RF_RT;
		cmd.ALU_OP <= AO_and;
		cmd.RF_sel <= RFS_RD;
		cmd.RF_we <= true;
		state_d <= S_Fetch;
		cmd.mem_ce <= true;
     when S_ANDI =>
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_IR_imm16;
		cmd.ALU_OP <= AO_and;
		cmd.RF_Sel <= RFS_RT;
		cmd.RF_we <= true;
		state_d <= S_Fetch;
		cmd.mem_ce <= true;
		
     when S_XOR => 
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_RF_RT;
		cmd.ALU_OP <= AO_xor;
		cmd.RF_sel <= RFS_RD;
		cmd.RF_we <= true;
		state_d <= S_Fetch;
		cmd.mem_ce <= true;
     when S_OR => 
		cmd.ALU_X_sel <= UXS_RF_RS;
		cmd.ALU_Y_sel <= UYS_RF_RT;
		cmd.ALU_OP <= AO_or;
		cmd.RF_sel <= RFS_RD;
		cmd.RF_we <= true;
		state_d <= S_Fetch;
		cmd.mem_ce <= true;
     when S_J =>
		
		cmd.ALU_X_sel <= UXS_PC_up;
		cmd.ALU_Y_sel <= UYS_IR_imm16;
		cmd.ALU_OP <= AO_plus;
		cmd.PC_we <= true;
		state_d <= S_Fetch_wait;
		cmd.mem_ce <= true;
     when S_SLL =>
		cmd.ALU_X_sel <= UXS_IR_SH;
        	cmd.ALU_Y_sel <= UYS_RF_RT;
        	cmd.ALU_OP <= AO_SLL;
        	cmd.RF_Sel <= RFS_RT;
        	cmd.RF_we <= true;
        	state_d <= S_Fetch;
        	cmd.mem_ce <= true;
		
		

      when others => null;
                     
    end case;
  end process FSM_comb;
end RTL;
