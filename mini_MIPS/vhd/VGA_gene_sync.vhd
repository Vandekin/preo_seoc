-- Auteur original: S. Viardot: modif S. Mancini
-- la synchro est modifiée pour une horloge à 25Mhz
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Trame 640*480

entity VGA_gene_sync is
  Port (
    clk   : in std_logic;
    rst : in std_logic;
    hsync : out std_logic;
    vsync : out std_logic;
    img   : out std_logic;
    x     : out std_logic_vector(9 downto 0);
    y     : out std_logic_vector(8 downto 0)
    );
end VGA_gene_sync;

architecture Behavioral of VGA_gene_sync is
  signal comptX, x_d           : unsigned(10 downto 0);
  signal comptY, y_d           : unsigned(9 downto 0);
  constant x_z            : unsigned(10 downto 0) := (others=>'0');
  constant y_z            : unsigned(9 downto 0) := (others=>'0');
  signal hsync_d, vsync_d : std_logic;
  signal imgx, imgy       : std_logic;

begin
  
  
  synchrone:  process (clk)
  begin 
    if rising_edge(CLK)  then
      if rst='1' then
	comptX <= x_z;
	comptY <= y_z;
	hsync <= '0';
	vsync <= '0';
	img <= '0';
	x <= (others=>'0');
	y <= (others=>'0');
      else
	if comptX<800 then 
	  comptX<=comptX+1;
	else 
	  comptX<=(others=>'0');
	end if;
	if comptX=0 then 
	  if comptY<521 then 
	    comptY<=comptY+1;
	  else 
	    comptY<=(others=>'0');
	  end if;
	end if;
	x <= std_logic_vector(x_d(9 downto 0));
	y <= std_logic_vector(y_d(8 downto 0));
	hsync <= hsync_d;
	vsync <= vsync_d;
	img <= imgx and imgy;
      end if;
    end if;
  end process;


  comb_x:  process (comptX)
  begin 
    x_d <= x_z;
    imgx <='0';
    hsync_d <= '0';
    if (comptx<96) then 
      x_d <= x_z;
      hsync_d <= '0';
    else 
      if (comptx<(96+48)) then 
	x_d <= x_z;
	hsync_d <= '1';
	imgx <= '0';
      else 
	if (comptx<(96+48+640)) then 
	  x_d <= comptx-96-48;
	  hsync_d <= '1';
	  imgx <= '1';
	else 
	  x_d <= x_z;
	  hsync_d <= '1';
	  imgx<='0';
	end if;		
      end if;
    end if;
  end process;

  comb_y :  process (comptY)
  begin 
    y_d <= y_z;
    vsync_d <= '0';
    imgy <= '0';
    
    if (compty<2) then 
      y_d <= y_z;
      vsync_d <= '0';
    else 
      if (compty<(2+29)) then 
	y_d <= y_z;
	vsync_d <= '1';
	imgy <= '0';
	
      else 
	if (compty<(2+29+480)) then 
	  y_d <= compty-2-29;
	  vsync_d <= '1';
	  imgy <= '1';
	else 
	  y_d <= y_z;
	  vsync_d <= '1';
	  imgy <= '0';
	end if;		
      end if;
    end if;
  end process;

end Behavioral;

