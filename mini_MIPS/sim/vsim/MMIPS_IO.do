onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:pc:clk
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:pc:rst
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:pc:cmd
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:pc:status
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:pc:state_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:pc:state_q
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:clk
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:rst
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:cmd
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:status
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:irq
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:mem_addr
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:mem_datain
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:mem_dataout
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:mem_we
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:mem_ce
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:pout
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:pout_valid
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:epc_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:epc_q
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:pc_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:pc_q
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:rf_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:rf_q
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:ad_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:ad_q
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:dt_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:dt_q
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:ir_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:ir_q
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:sr_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:sr_q
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:it_d
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:it_q
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:x
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:y
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:alu_res
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:pout_d
add wave -noupdate -format Literal :tb_mmips_io:c_mmips_io:c_proc:po:pout_q
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:pout_valid_d
add wave -noupdate -format Logic :tb_mmips_io:c_mmips_io:c_proc:po:pout_valid_q
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1 ns}
