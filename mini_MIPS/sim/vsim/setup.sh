export modelsim=/softslin/modelsim10_0b/modeltech/
export PATH=$PATH:$modelsim/bin:

XILINX=/softlin06/XILINX/ise_101i/ISE
export XILINX
XILINX_LIB=$(XILINX)/vhdl/lib/simprims_100b

XILINX_EDK=/softlin06/XILINX/edk_101i/EDK
export XILINX_EDK
PLATFORM=lin

XILINX_EDK_LIB=$XILINX_EDK/hw/lib_100b/
export XILINX_EDK_LIB

LMC_HOME=${XILINX}/smartmodel/${PLATFORM}/installed_${PLATFORM}
export LMC_HOME


if [ -n "$PATH" ]
then
   PATH=${PATH}:${XILINX}/bin/${PLATFORM}:${LMC_HOME}/bin
else
   PATH=${XILINX}/bin/${PLATFORM}:${LMC_HOME}/bin
fi
export PATH

if [ -n "$LD_LIBRARY_PATH" ]
then
   LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/X11R6/lib:${XILINX}/bin/${PLATFORM}:${XILINX}/lib/${PLATFORM}:${LMC_HOME}/lib/sun4Solaris.lib
else
   LD_LIBRARY_PATH=${XILINX}/bin/${PLATFORM}:${XILINX}/lib/${PLATFORM}:${LMC_HOME}/lib/sun4Solaris.lib:/usr/X11R6/lib
fi
export LD_LIBRARY_PATH

if [ -n "$NPX_PLUGIN_PATH" ]
then
   NPX_PLUGIN_PATH=${NPX_PLUGIN_PATH}:${XILINX}/java/${PLATFORM}/jre/plugin/i386/ns4
else
   NPX_PLUGIN_PATH=${XILINX}/java/${PLATFORM}/jre/plugin/i386/ns4
fi
export NPX_PLUGIN_PATH



if [ -n "$PATH" ]
then
   PATH=${PATH}:${XILINX_EDK}/bin/${PLATFORM}:${XILINX_EDK}/gnu/powerpc-eabi/${PLATFORM}/bin/
else
   PATH=${XILINX_EDK}/bin/${PLATFORM}:${XILINX_EDK}/gnu/powerpc-eabi/${PLATFORM}/bin/
fi
export PATH

if [ -n "$LD_LIBRARY_PATH" ]
then
   LD_LIBRARY_PATH=/usr/X11R6/lib:${LD_LIBRARY_PATH}:${XILINX_EDK}/bin/${PLATFORM}:${XILINX_EDK}/lib/${PLATFORM}
else
   LD_LIBRARY_PATH=${XILINX_EDK}/bin/${PLATFORM}:${XILINX_EDK}/lib/${PLATFORM}:/usr/X11R6/lib
fi
export LD_LIBRARY_PATH
