.text
	ori $2, $2, 0x0000
	ori $1, $1, 0x0000
	ori $5, $5, 0x0006

loop:	add $1, $1, $2
	addi $2, $2, 0x0001
	beq $2, $5, end
	beq $2, $2, loop

end:	lui $3, 0xFFFF
